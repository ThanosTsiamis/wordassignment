package wordFunctions;

import java.util.*;

import static java.util.stream.Collectors.toMap;

public class WordFunctions {


    private static boolean isCommonWord(String word) {
        String[] commonWords = {"the", "-–", "–", "–-", "of", "and", "a", "to", "in", "is", "you", "that", "it", "he", "was", "for", "on", "are", "as", "with", "his", "they", "i", "at", "be", "this", "have", "from", "or", "one", "had", "by", "word", "but", "not", "what", "all", "were", "we", "when", "your", "can", "said", "there", "use", "an", "each", "which", "she", "do", "how", "their", "if", "will", "up", "other", "about", "out", "many", "then", "them", "these", "so", "some", "her", "would", "make", "like", "him", "into", "time", "has", "look", "two", "more", "write", "go", "see", "number", "no", "way", "could", "people", "my", "than", "first", "water", "been", "call", "who", "oil", "its", "now", "find", "long", "down", "day", "did", "get", "come", "made", "may", "part", "our"};


        Set<String> commonWordSet = new HashSet<>(Arrays.asList(commonWords));
        return commonWordSet.contains(word);

    }

    private static HashMap<String, Integer> orderTheMap(HashMap<String, Integer> wordMap) {
        return wordMap.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));
    }


    public static HashMap<String, Integer> findMostOccurringTenPercentOfWordsWithOccurences(HashMap<String, Integer> wordMap) {
        HashMap<String, Integer> ordered = orderTheMap(wordMap);
        HashMap<String, Integer> returnMap = new HashMap<>();

        int pointing = 0;
        String word = (String) ordered.keySet().toArray()[pointing];

        for (int i = 0; i < Math.floor(0.1 * ordered.size()); i++) {

            if (isCommonWord(word)) {
                pointing += 1;
                word = (String) ordered.keySet().toArray()[pointing];
            } else {
                if (returnMap.containsKey(word)) {
                    returnMap.replace(word, returnMap.get(word), returnMap.get(word) + ordered.get(word));
                    pointing += 1;
                    word = (String) ordered.keySet().toArray()[pointing];

                } else {
                    returnMap.put(word, ordered.get(word));
                    pointing += 1;
                    word = (String) ordered.keySet().toArray()[pointing];
                }
            }
        }
        return returnMap;

    }

    public static HashMap<String, Integer> findEveryWordInAMap(HashMap<String, Integer> wordMap) {
        HashMap<String, Integer> ordered = orderTheMap(wordMap);
        HashMap<String, Integer> returnMap = new HashMap<>();
        int pointing = 0;
        String word = (String) ordered.keySet().toArray()[pointing];
        for (int i = 0; i < Math.floor(0.9999999999999999 * ordered.size()); i++) {


            if (returnMap.containsKey(word)) {
                returnMap.replace(word, returnMap.get(word), returnMap.get(word) + ordered.get(word));
                pointing += 1;
                word = (String) ordered.keySet().toArray()[pointing];

            } else {
                returnMap.put(word, ordered.get(word));
                pointing += 1;
                word = (String) ordered.keySet().toArray()[pointing];
            }

        }
        return returnMap;

    }


}
