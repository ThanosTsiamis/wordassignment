package importTextHelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static wordFunctions.WordFunctions.findEveryWordInAMap;
import static wordFunctions.WordFunctions.findMostOccurringTenPercentOfWordsWithOccurences;

public interface GetWords {
    static String[] getWords(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        StringBuilder president = new StringBuilder();
        String st;
        while ((st = br.readLine()) != null) {
            president.append(st);
        }
        return regex(president);
    }

    static HashMap<String, Integer> parseFilesForFivePercent(File[] files, HashMap<String, Integer> presidentWords) throws IOException {

        for (File file : files) {
            if (file.isDirectory()) {
                System.out.println("Directory: " + file.getName());
                parseFilesForFivePercent(file.listFiles(), presidentWords); // Calls same method again.
            } else {

                String[] words = getWords(file);
                AddFivePercentWithOccurences(presidentWords, words);
            }
        }
        return presidentWords;
    }

    static HashMap<String, Integer> parseFilesForAddingEveryWord(File[] files, HashMap<String, Integer> presidentWords) throws IOException {

        for (File file : files) {
            if (file.isDirectory()) {
                System.out.println("Directory: " + file.getName());
                parseFilesForAddingEveryWord(file.listFiles(), presidentWords); // Calls same method again.
            } else {

                String[] words = getWords(file);
                AddEveryWord(presidentWords, words);
            }
        }
        return presidentWords;
    }


    static HashMap<String, Integer> AddFivePercentWithOccurences(HashMap<String, Integer> presidentWordsWithOccurences, String[] words) {
        List<String> wordList = Arrays.asList(words);
        HashMap<String, Integer> wordMap;
        wordMap = listToHashMap(wordList);
        HashMap<String, Integer> newPresidentWords = findMostOccurringTenPercentOfWordsWithOccurences(wordMap);

        presidentWordsWithOccurences.putAll(newPresidentWords);

        return presidentWordsWithOccurences;


    }

    static HashMap<String, Integer> AddEveryWord(HashMap<String, Integer> presidentWordsWithOccurences, String[] words) {
        List<String> wordList = Arrays.asList(words);
        HashMap<String, Integer> wordMap;
        wordMap = listToHashMap(wordList);
        HashMap<String, Integer> newPresidentWords = findEveryWordInAMap(wordMap);
        presidentWordsWithOccurences.putAll(newPresidentWords);

        return presidentWordsWithOccurences;
    }

    private static String[] regex(StringBuilder wordToBeRegexed) {
        //.replace("\\A\"[a-z] ","")
        return wordToBeRegexed.toString().toLowerCase().replaceAll("\"", " ").replace("'", " ").replace(";", " ").replace("?", " ").replace("\uFFFD", "").replace(" \" ", "").replace("[applause]", "").replace("[laugh", " ").replace("[inaudible]", " ").replace("[laughter]", "").replace("audience:", "").replace(".", " ").replace("president:", "").replace("—", " ").replace(",", " ").replace("-", " ").replace("(Applause)", "").replace("--", "").replace("(laughter)", "").replace("�", "").replace("�", " ").replace("-", " ").replace("[i", " ").replace("(applause)", "").split("\\s+");
    }
    private static HashMap<String, Integer> listToHashMap(List<String> wordList) {
        HashMap<String, Integer> wordMap = new HashMap<>();
        for (int i = 0; i < wordList.size(); i++) {
            int count = 1;
            for (int j = 0; j < wordList.size(); j++) {
                if (wordList.get(i).equals(wordList.get(j)) && i != j) {
                    count += 1;
                }

            }


            wordMap.put(wordList.get(i), count);


        }

        return wordMap;
    }

}
